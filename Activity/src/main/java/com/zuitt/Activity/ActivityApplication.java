package com.zuitt.Activity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
@RequestMapping("/greeting")

public class ActivityApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActivityApplication.class, args);
	}

	@GetMapping("/hello")
	public String hello(){
		return "Hello World";
	}

	@GetMapping("/hi")
	public String hi(@RequestParam(value="name", defaultValue = "John") String name){
		return String.format("Hi %s", name);
	}

	@GetMapping("/friend")
	public String friend(@RequestParam(value="name", defaultValue = "Joe") String name, @RequestParam(value = "friend", defaultValue = "Mama") String friend) {
		return String.format("Hello %s! My name is %s.", friend, name);
	}

	@GetMapping("/hello/{name}")
	public String greetFriend(@PathVariable("name") String name) {
		return String.format("Nice to meet you %s", name);
	}

	@PostMapping("/enroll")   public String enroll(@RequestParam(value="user") String user) {
		enrollees.add(user);
		return String.format("Welcome %s!", user);
	}

	@GetMapping("/getEnrollees")
	public String getEnrollees() {
		return enrollees.toString();
	}

	@GetMapping("/nameage")
	public String nameAge(@RequestParam(value="name") String name, @RequestParam(value="age") String age) {
		return String.format("Hello %s! My age is %s.", name, age);
	}

	@GetMapping("/courses/{id}")
	public String getCourses(@PathVariable("id") String id) {
		switch (id) {
			case "java101":
				return "Java 101, MWF 8:00AM-11:00 AM, PHP 3000.00";
			case "sql101":
				return "SQL 101, TTH 1:00PM-4:00 PM, PHP 2000.00";
			case "javaee101":
				return "Java EE 101, MWF 1:00PM-4:00 PM, PHP 3500.00";
			default:
				return "Course not found.";      }
	}
}
